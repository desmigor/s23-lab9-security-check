# Lab 9 - Software Quality and Reliability

Testing manually [https://dribbble.com/](https://dribbble.com/) using [OWASP CSS (OWASP Cheat Sheet Series)](https://cheatsheetseries.owasp.org/index.html) 

## Testing 1 - Forgot Password

**Link**: [Forgot Password](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html)

**Requirement**: Return a consistent message for both existent and non-existent accounts.

| Step                                                 | Result                                                                                                                     |
| ---------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| Open dribble.com                                     | OK                                                                                                                         |
| Click the Sign In button in the header               | OK, navigated to [Sign in page](https://dribbble.com/session/new)                                                          |
| Click on "Forgot Password?"                          | OK, navigated to [reset password page](https://dribbble.com/password_resets/new)                                           |
| Entered a random non-existing email                  | OK, displayed a generic message about recovery email being sent if the account exists, and navigates back to Sign In Page. |
| Entered an existing email                            | OK, displayed a generic message about recovery email being sent if the account exists, and navigates back to Sign In Page. |

**Verdict**: ✅ Passed - The message displayed for existing and non-existing users is the same.


## Testing 2 - Authentication

**Link**: [Authentication](https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html#user-ids)

**Requirement**: Make sure your usernames/user IDs are case-insensitive

| Step                                                 | Result                                                                                                                     |
| ---------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| Open dribble.com                                     | OK                                                                                                                         |
| Click the Sign In button in the header               | OK, navigated to [Sign in page](https://dribbble.com/session/new)                                                          |
| Enter an username in Small letters and password      | OK, successfully signed in                                                                                                 |
| Enter an username in Capital letters and password    | OK, successfully signed in                                                                                                 |
| Enter an username in Camel case and password         | OK, successfully signed in                                                                                                 |

**Verdict**: ✅ Passed - Username in different cases allows a user to sign in


## Testing 3 -  File Upload (Allowed extensions)

**Link**: [File Upload](https://cheatsheetseries.owasp.org/cheatsheets/File_Upload_Cheat_Sheet.html)

**Requirement**: List allowed extensions

| Step                                                 | Result                                                                                                                     |
| ---------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| Open dribble.com                                     | OK                                                                                                                         |
| Click the Sign In button in the header               | OK, navigated to [Sign in page](https://dribbble.com/session/new)                                                          |
| Click on "Upload" Button                             | OK, successfully goes to [Upload page](https://dribbble.com/uploads/new)                                                   |
| View allowed extensions                              | OK, they mention `png`, `jpeg`, `gif`, `mp4`                                                                               |
| Upload an `svg` file                                 | Failure, got a message that says `This file type is not allowed here`                                                      |

**Verdict**:  ✅ Passed - Only allowed file types are accepted


## Testing 4 -  File Upload (File size limit)

**Link**:  [File Upload](https://cheatsheetseries.owasp.org/cheatsheets/File_Upload_Cheat_Sheet.html)

**Requirement**: Set a file size limit

| Step                                                 | Result                                                                                                                     |
| ---------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------- |
| Open dribble.com                                     | OK                                                                                                                         |
| Click the Sign In button in the header               | OK, navigated to [Sign in page](https://dribbble.com/session/new)                                                          |
| Click on "Upload" Button                             | OK, successfully goes to [Upload page](https://dribbble.com/uploads/new)                                                   |
| View accepted file sizes                             | OK, they mention `Minimum 1600px width recommended. Max 10MB each (20MB for videos)`                                       |
| Upload a video with size `34.6 MB`                   | Failure, got a message that says `File needs to be under 20 MB`                                                            |

**Verdict**:  ✅ Passed - Only allows files with accepted sizes.
